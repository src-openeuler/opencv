# opencv

#### 介绍
OpenCV means Intel® Open Source Computer Vision Library.  
1.  功能  
    OpenCV是一个基于BSD许可（开源）发行的跨平台计算机视觉库，可以运行在Linux、Windows、Android和Mac OS操作系统上。它轻量级而且高效————由一系列C函数和少量C++类构成，同时提供了Python、Ruby、MATLAB等语言的接口，实现了图像处理和计算机视觉方面的很多通用算法。  
    OpenCV用C++语言编写，它的主要接口也是C++语言，但是依然保留了大量的C语言接口。在计算机视觉项目的开发中，OpenCV作为较大众的开源库，拥有了丰富的常用图像处理函数库，采用C/C++语言编写，可以运行在Linux/Windows/Mac等操作系统上，能够快速的实现一些图像处理和识别的任务。  
2.  约束  
    由于Opencv是基于BSD许可协议，开发者可以自由使用、修改源码，也可以将修改后的代码作为开源或者专有软件再发布。只要是Opencv提供的函数都可以使用，不过可靠性和安全性一般。  

#### 软件架构
软件架构说明  
      * OpenCV的组织关系  
      ![opencv](./docs/opencv.png)  
      * OpenCV架构  
      ![opencv_module](./docs/opencv_module.png)  
    

#### 安装教程

1.  环境准备  
    安装C++编译器和构建工具。在*NIX平台上，它通常是GCC/G++或Clang编译器以及Make或NInja构建工具。在Windows上，它可以是Visual Studio IDE或MinGW-w64编译器。Android NDK中提供了适用于Android的本机工具链。XCode IDE用于为OSX和iOS平台构建软件。  
    从官方网站或其他来源安装CMake。  
    获取其他第三方依赖项：具有额外功能的库，例如解码视频或显示GUI元素；提供所选算法的优化实现的库；用于文档生成和其他附加功能的工具。检查 [OpenCV配置选项参考](https://docs.opencv.org/4.5.2/db/d05/tutorial_config_reference.html) 以获取可用选项和相应的依赖项。  
2.  获取源码  
    典型的软件项目由一个或多个代码库组成。OpenCV有两个带有代码的存储库：opencv主存储库，具有稳定且积极支持的算法和opencv_contrib，其中包含实验性和非免费（专利）算法；和一个带有测试数据的存储库：opencv_extra。  
    可从以下网址下载：  
        * 转到<https://github.com/opencv/opencv/releases>并从任何版本下载“源代码”存档。  
        * （可选）转到<https://github.com/opencv/opencv_contrib/releases>并下载与opencv相同版本的“源代码”存档。  
        * （可选）转到<https://github.com/opencv/opencv_extra/releases>并下载与opencv相同版本的“源代码”存档。  
    也可以通过git下载代码的方式：  
        git clone <https://github.com/opencv/opencv>  
        git clone https://github.com/opencv/opencv_contrib  
        git clone https://github.com/opencv/opencv_extra

#### 使用说明

1.  配置  
    CMake将验证所有必要的工具和依赖项是否可用并与库兼容，并将为所选构建系统生成中间文件。可以是Makefiles、IDE项目和解决方案等。通常这一步是在新建的build目录下进行的。  
    cmake -G<generator> <configuration-options> <source-directory>
2.  构建  
    在构建过程中，源文件被编译成目标文件，这些目标文件链接在一起或以其他方式组合成库和应用程序。这一步可以使用通用命令运行：  
    cmake --build <build-directory> <build-options>  
    或者可以直接调用底层构建系统：  
    make
3.  安装  
    在安装过程中，构建结果和构建目录中的其他文件将被复制到安装位置。默认安装位置/user/local在UNIX和C:/Program FilesWindows上。可以在配置步骤中通过设置CMAKE_INSTALL_PREFIX选项更改此位置。要执行安装，请运行一下命令：  
    cmake --build <build-directory> --target install <other-options>

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
